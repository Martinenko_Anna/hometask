import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.*;

public class AutoTest {

    //Базовый УРЛ будет использоваться по умолчанию в автотесте
    //И вызываться в аннотации @Test
    String baseUrl = "http://1:1@provse.wezom.net/";
    final private static String contact = "Иван Иванов";
    final static String Email = "test@test.te";
    final static String Massage = "massage in this form";



     //Аннотакции TestNG (говорит что мы будем делать до выполнения теста)
     //В данном случае мы внесем следующие настройки в метод startBrowser


    @BeforeTest
    public void startBrowser(){

        //Инициализация хром драйвера (запускать хром. Так же можно указать firefox,opera,ie,safari (нужно качать отдельный драйвер))
        Configuration.browser="chrome";

        //Указывается путь где лежит наш chromedriver.exe
        //webdriver.chrome.driver (использовать хром драйвер)
        //C:/chromedriver.exe (путь к нашему драйверу)
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");

        //Сброс куков перед выполнением теста
        clearBrowserCookies();
    }


     //@Test в данной аннотации мы будем говорить какие функции использовать


    @Test
    public void runTest(){
        //функция open в селениде работает как в селениуме driver.get(baseUrl)
        //baseUrl у нас указан выше в параметре String (вне метода)
        //Тоисть открой "http://google.ru"
        open(baseUrl);
        //sleep(2000);
        $(By.xpath("//ul[@class=\"footer-menu\"]//a[text() = \"Контакты\"]")).scrollTo().click();
        //$x("/html/body/div[1]/div[2]/div[2]/div[1]/div/div[2]/ul/li[4]/a").scrollTo().click();
        //$(byName("contact_name")).setValue("Тестовый юзер");
///11111
        $(By.xpath("//div[@data-ajax=\"contacts\"]//input[@name=\"contact_name\"]")).setValue(contact);
        //$(By.xpath("html/body/div[1]/div[2]/div[1]/div[2]/div[1]/div/div/div[6]/div[1]/div[2]/div/input")).setValue(contact);

        $(By.xpath("//div[@data-ajax=\"contacts\"]//input[@name=\"contact_email\"]")).setValue(Email);
        //$(By.xpath("html/body/div[1]/div[2]/div[1]/div[2]/div[1]/div/div/div[6]/div[2]/div[2]/div/input")).setValue(Email);


        $(By.xpath("//div[@data-ajax=\"contacts\"]//textarea[@name=\"contact_text\"]")).setValue(Massage);
        //$(By.xpath("html/body/div[1]/div[2]/div[1]/div[2]/div[1]/div/div/div[6]/div[3]/div[2]/div/textarea")).setValue(Massage);

        $(By.xpath("//div[@data-ajax=\"contacts\"]//button[@type=\"submit\"]")).click();
        //$(By.xpath("html/body/div[1]/div[2]/div[1]/div[2]/div[1]/div/div/div[6]/div[4]/div/button")).click();
        $(By.cssSelector(".popup-window__title")).shouldHave(Condition.text("Спасибо за сообщение! Мы свяжемся с Вами в ближайшее время!"));


                /*$(By.xpath("/html/body/div[2]/div/div[1]/div/div[2]/div[1]/div[1]/p")).shouldHave(Condition.text("Производится вызов. Подождите пока Ваш телефон зазвонит!\n" +
                "Если телефон не позвонил в течении минуты, попробуйте ещё раз."));
                * */

        sleep(4000);
    }
}